import axios from "axios"
import {
    FETCH_TODOS_REQUEST,
    FETCH_TODOS_SUCCESS,
    FETCH_TODOS_ERROR,
    FETCH_TODOS_ADD_TODO
} from "./todoTypes"


const fetchTodoRequest = () =>{
    return{
        type:FETCH_TODOS_REQUEST
    }
}

const fetchTodoSuccess = todos =>{
    return{
        type:FETCH_TODOS_SUCCESS,
        payload:todos
    }
}
export const AddTodoSuccess = todos =>{
    return{
        type:FETCH_TODOS_ADD_TODO,
        payload: todos
    }
}
const fetchTodoError = error =>{
    return{
        type:FETCH_TODOS_ERROR,
        payload:error
    }
}

export const fetchtodos = () =>{
return (dispatch) => {
    try {
        dispatch(fetchTodoRequest)
     axios.get('https://todoserver-app.herokuapp.com/Todo')
        .then(response =>{
            const todos = response.data
            console.log({todos})
            dispatch(fetchTodoSuccess(todos))
        })
    } catch (error)  {
        const errorMsg = error.message
        dispatch(fetchTodoError(errorMsg))
    } 

}
}