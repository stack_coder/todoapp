import {
    FETCH_TODOS_REQUEST,
    FETCH_TODOS_SUCCESS,
    FETCH_TODOS_ERROR,
    FETCH_TODOS_ADD_TODO
} from "./todoTypes"

const initialState = {
    loading:false,
    todos:[],
    error:''
}  

const todoreducer = (state = initialState,action) => {
    switch (action.type) {
        case FETCH_TODOS_REQUEST:
            return {
                ...state,
                loading: true
            }
            case FETCH_TODOS_SUCCESS:
                return {
                    loading: false,
                    todos: action.payload,
                    error:''
                }
                case FETCH_TODOS_ADD_TODO:
                    return {
                        loading: false,
                        users: action.payload,
                        error:''
                    }
                case FETCH_TODOS_ERROR:
                    return{
                        loading: false,
                        todos: [],
                        error:action.payload
                    }
                    default:return state

                } 
    }

export default todoreducer