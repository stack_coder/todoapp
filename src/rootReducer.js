import { combineReducers } from 'redux'
import todoreducer from './Todo/todoReducer'
import userReducer from "./user/userReducer"

const rootReducer = combineReducers({
    user: userReducer,
    todo: todoreducer
})

export default rootReducer