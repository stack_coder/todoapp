import React,{useState,useEffect} from 'react';
import './App.css';
import TodoList from './components/TodoList';
import axios from "axios"
import { isAuthenticated } from './components/authapi';
import Menu from './components/Menu';
import { Redirect } from 'react-router-dom';


function App() {

const[items,setitem] = useState([])
const[call,setcall] =useState([])

const data = isAuthenticated()
const {user,token} = data
const userdata = JSON.parse(user)

console.log({items})

const getCall = async () => {
  try {
    const response = await axios.get(
      `https://todoserver-app.herokuapp.com/Todo/`
    );
    //  console.log(response.data) 
    setcall(response.data);
  } catch (error) {
    console.log(error);
  }
};
console.log({items})
useEffect(() => {
  getCall();
}, []);



const getData = async () => {
  try {
    const response = await axios.get(
      `https://cors-anywhere.herokuapp.com/https://todoserver-app.herokuapp.com/Todo/${userdata._id}`
    );
    //  console.log(response.data) 
    setitem(response.data);
  } catch (error) {
    console.log(error);
  }
};
console.log({items})
useEffect(() => {
  getData();
}, []);

const performRedirect = () => {
  //TODO: do a redirect here
  
  if (isAuthenticated().user == null) {
    return <Redirect to="/" />;
  }
};

  return (
    <div>
      <Menu />
{performRedirect()}
     
    <div className='todo-app'>
    <TodoList items={items} />
    {/* <TodoList items={call}/> */}
    </div>
    {/* <div className='todo-app'>
    {/* <TodoList items={items}/> 
     <TodoList items={call} para={"What is going on Around You"}/> 
    
    </div> */}
    </div>
  );
}

export default App;
