import axios from "axios"
import {
    FETCH_USERS_REQUEST,
    FETCH_USERS_SUCCESS,
    FETCH_USERS_ERROR,
    FETCH_USERS_ADD_USER
} from "./userTypes"


const fetchUserRequest = () =>{
    return{
        type:FETCH_USERS_REQUEST
    }
}

const fetchUserSuccess = users =>{
    return{
        type:FETCH_USERS_SUCCESS,
        payload: users
    }
}
export const AddUserSuccess = users =>{
    return{
        type:FETCH_USERS_ADD_USER,
        payload: users
    }
}
const fetchUserError = error =>{
    return{
        type:FETCH_USERS_ERROR,
        payload:error
    }
}

export const fetchUsers = () =>{
return (dispatch) =>{
    dispatch(fetchUserRequest)
  axios.get('https://todoserver-app.herokuapp.com/User')
  .then(response =>{
      const users = response.data
      console.log({users})
      dispatch(fetchUserSuccess(users))
  })
  .catch(error => {
      const errorMsg = error.message
      dispatch(fetchUserError(errorMsg))
  })
}
}

