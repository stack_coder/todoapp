import React from "react";
import { Provider } from 'react-redux'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./components/Home";
import Signup from "./components/Signup";
import Signin from "./components/Signin";
import App from "./App";
import store from "./store"


const Routes = () => {
  return (

    <BrowserRouter>
   <Provider store = {store}>
      <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/App"  component={App} />
        <Route path="/signup"  component={Signup} />
        <Route path="/signin"  component={Signin} />
      </Switch>
      </Provider>
    </BrowserRouter>
  );
};

export default Routes;