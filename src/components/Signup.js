import { Link } from '@material-ui/core';
import React, { useState } from 'react'
import { Redirect } from 'react-router-dom';
import { isAuthenticated, signup } from './authapi'
import { useDispatch, useSelector } from 'react-redux'
import Menu from './Menu'
import {AddUserSuccess} from "../user/userActions"

const Signup = () => {

    const [values,setValues] = useState({
        Username:"",
        email:"",
        password:"",
        error:"",
        success:false
    })
     
    const {Username,email,password,error,success} = values;

const handleChange = Username => event =>{
    setValues({...values,error:false,[Username]: event.target.value })
}

const dispatch = useDispatch();

const onSubmit = event =>{ 
    try {
      event.preventDefault()
    setValues({...values,error:false})
    signup({ Username,email,password})
    const users = {Username,email,password}
    console.log({users})
    dispatch(AddUserSuccess( users))
    const data = {...values}
    console.log({data})
    if(data.error){
      setValues({ ...values,error:data.error,success:false})
  }else{
      setValues({
          ...values,
          Username:"",
          email:"",
          password:"",
          error:"",
          success:true
      })
  }
    } catch (error) {
      console.log("error in signup")
    }
}

    const signUpForm = () => {
        return(
            <div className="row">
        <div className="col-md-6 offset-sm-3 text-left">
          <form>
            <div className="form-group">
              <label className="text-light">UserName</label>
              <input
                onChange={handleChange("Username")}
                className="form-control"
                type="text"
                value={Username}
              />
            </div>
            <div className="form-group">
              <label className="text-light">Email</label>
              <input
                onChange={handleChange("email")}
                className="form-control"
                type="email"
                value={email}
              />
            </div>

            <div className="form-group">
              <label className="text-light">Password</label>
              <input
                onChange={handleChange("password")}
                className="form-control"
                type="password"
                value={password}
              />
            </div>
            <button onClick={onSubmit} className="btn btn-success btn-block">
              Register
            </button>
          </form>
        </div>
      </div>
        )
    }

    const successMessage = () => {
        return (
          <div className="row">
            <div className="col-md-6 offset-sm-3 text-left">
              <div
                className="alert alert-success"
                style={{ display: success ? "" : "none" }}
              >
                New account was created successfully. Please{" "}
                <Link to="/Signin">Login Here</Link>
              </div>
            </div>
          </div>
        );
      };

      const performRedirect = () => {
        //TODO: do a redirect here
        
        if (isAuthenticated().user) {
          return <Redirect to="/" />;
        }
      };
      

      const errorMessage = () => {
        return (
          <div className="row">
            <div className="col-md-6 offset-sm-3 text-left">
              <div
                className="alert alert-danger"
                style={{ display: error ? "" : "none" }}
              >
                {error}
              </div>
            </div>
          </div>
        );
      };

    return (
        <div>
          <Menu />
            {successMessage()}
            {errorMessage()}
            {signUpForm()}
            {performRedirect()}
        </div>
    )
}

export default  Signup;