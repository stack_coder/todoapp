import {API} from "./backend"

import { useHistory } from "react-router-dom";



export const signup = async user =>{
try {
    const res = await fetch(`https://todoserver-app.herokuapp.com/User`,{
        method:"POST",
        "headers":{
            Accept:"application/json",
            "Content-Type":"application/json"
        },
        body:JSON.stringify(user)
    })
    return res.json();
} catch (error) {
    console.log(error)
}
}
export const signin = async user =>{
    try {
        const res = await fetch(`https://todoserver-app.herokuapp.com/signin`,{
            method:"POST",
            "headers":{
                Accept:"application/json",
                "Content-Type":"application/json"
            },
            body:JSON.stringify(user)
        })
        return res.json();
    } catch (error) {
        console.log(error)
    }  
}

  
  export const authenticate = (data,next) =>{
      if(typeof window !== "undefined"){
         
          const{user,token} = data
        //  console.log({user,token})
          
          localStorage.setItem("user",JSON.stringify(user))
          localStorage.setItem("token",JSON.stringify(token))

          next();
      }
  }


  export const   signout  = async next => {
    if (typeof window !== "undefined") {
        localStorage.removeItem("token")
        localStorage.removeItem("user")

        next();
try {
    const sign =  await fetch(`https://todoserver-app.herokuapp.com/signout`,{
        method:"GET"
                })
                console.log(sign)
              //  history.push("/Signin");
} catch (error) {
     console.log(error)
}
// return 
//history.push("/login");
    
    }
  };

  export const isAuthenticated = () =>{
  
const data = {}

const user = localStorage.getItem("user")
const token = localStorage.getItem("token")
//console.log({user})
//console.log({token})

if(user){
    data.user = user
}
if(token){
    data.token = token
}
return data
  }
//     if(localStorage.getItem("user")){
//         return JSON.parse(localStorage.getItem("user"));
//     } 
//     if(localStorage.getItem("token")){
//         return JSON.parse(localStorage.getItem("token"));
//     } 
//     else{
// return false;
//     }
  