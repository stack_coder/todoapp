import React, { useState, useEffect, useRef } from 'react';
import { isAuthenticated } from './authapi';


const TodoForm =({ edit,onSubmit})  =>{
  const [input, setInput] = useState(edit ? edit.value : '');

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  });

const data = isAuthenticated()
const {user,token} = data
const userdata = JSON.parse(user)
// console.log({user})
// console.log({token})
//console.log({user})
console.log({userdata});
console.log(typeof user)

const formtodo = (userId,token, todos) => {
   return fetch(`https://todoserver-app.herokuapp.com/Todo/${userId}`,{
    method:"POST",
    headers:{
     Accept:"application/json",
     "Content-Type":"application/json",
     Authorization: `Bearer ${token}`
    },
    body:JSON.stringify(todos)
  })
  .then(res =>{
    return res.json();
  })
  .catch(err => console.log(err))
}

const formupdate = (id , todos) => {
  return fetch(`https://app-todosserver.herokuapp.com/${id}`,{
   method:"PATCH",
   headers:{
    Accept:"application/json",
     "Content-Type":"application/json"
   },
   body:JSON.stringify(todos)
 })
 .then(res =>{
   return res.json();
 })
 .catch(err => console.log(err))
}


  const handleChange = e => {
    setInput(e.target.value);
  };

  const handleSubmit = async e => {
    e.preventDefault();
    onSubmit({
      Name: input,
    });
    console.log(input)
    setInput('');
try {
  console.log(user._id)
  const result = await formtodo(userdata._id,token,{ Name: input })
  //console.log({user})
  console.log({result})
  if(result.error){
        setInput({...input,error:result.error})
      }
      else{
        setInput({
          ...input,
        Name:""
        })
      }
} catch (error) {
  console.log("error")
}
  }
  return (
    <form onSubmit={handleSubmit} className='todo-form'>
      {edit ? (
        <>
          <input
            placeholder='Update your item'
            value={input}
            onChange={handleChange}
            name='value'
            ref={inputRef}
            className='todo-input edit'
          />
          <button onClick={handleSubmit} className='todo-button edit'>
            Update
          </button>
        </>
      ) : (
        <>
          <input
            placeholder='Add a todo'
            value={input}
            onChange={handleChange}
            name='Name'
            className='todo-input'
            ref={inputRef}
          />
          <button onClick={handleSubmit} className='todo-button'>
            Add todo
          </button>
        </>
      )}
    </form>
  );
}

export default TodoForm;