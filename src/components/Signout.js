import React from 'react'
import { signout } from './authapi'

export default function Signout() {
    return (
        <div>
            {signout()}
        </div>
    )
}
