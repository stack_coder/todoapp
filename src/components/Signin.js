import React, { useState } from 'react'
import {Link,Redirect} from "react-router-dom"
import { authenticate, isAuthenticated, signin } from './authapi';
import Menu from './Menu'


const Signin = ()  =>{

const [values,setValues] =useState({
    email:"",
    password:"",
    error:"",
    success:"",
    loading:false,
    didRedirect:false
});

const {email,password,error,success,loading,didRedirect } = values;

//const{user} = isAuthenticatd();

const handleChange = Username => event =>{
    setValues({...values,error:false,[Username]: event.target.value })
}

const onSubmit = async event => {
event.preventDefault();
setValues({...values,error:false,loading:true})
try {
    const result = await signin({email,password})
  //  console.log({result})
    const {user,token} = result
   // console.log({user})
    //console.log({token})
    if(result.error){
        setValues({...values,error:false,loading:false})
            }
            else{
                authenticate(result ,() =>{
                    setValues({
                        ...values,
                        didRedirect:true
                    })
                })
            }
} catch (error) {
    
}
}

 const performRedirect = () => {
  //TODO: do a redirect here
  
  if (isAuthenticated().user) {
    return <Redirect to="/App" />;
  }
};

const loadingMessage = () => {
  return (
    loading && (
      <div className="alert alert-info">
        <h2>Loading...</h2>
      </div>
    )
  );
};

  const errorMessage = () => {
    return (
      <div className="row">
        <div className="col-md-6 offset-sm-3 text-left">
          <div
            className="alert alert-danger"
            style={{ display: error ? "" : "none" }}
          >
            {error}
          </div>
        </div>
      </div>
    );
  };


    const signInForm = () => {
        return(

          
            <div className="row">
        <div className="col-md-6 offset-sm-3 text-left">
          <form>
           
            <div className="form-group">
              <label className="text-light">Email</label>
              <input
                className="form-control"
                type="email"
                value={email}
                onChange={handleChange("email")}
              />
            </div>

            <div className="form-group">
              <label className="text-light">Password</label>
              <input
                className="form-control"
                type="password"
                onChange={handleChange("password")}
                value={password}

              />
            </div>
            <button  onClick={onSubmit} className="btn btn-success btn-block">
              Sigin in
            </button>
          </form>
        </div>
      </div>
        )
    }
    return (
        <div>
          <Menu />
          {loadingMessage()}
      {errorMessage()}
      {signInForm()}
      {performRedirect()}
        </div>
    )
}

export default Signin;